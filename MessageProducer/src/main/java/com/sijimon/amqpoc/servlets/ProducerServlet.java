package com.sijimon.amqpoc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.Queue;
import javax.jms.Topic;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Definition of the two JMS destinations used by the quickstart
 * (one queue and one topic).
 */

@JMSDestinationDefinitions(
    value = {
        @JMSDestinationDefinition(
            interfaceName = "javax.jms.Queue",
            name = "java:/HELLOWORLDMDBQueue",
            destinationName = "jms.queue.HelloWorldMDBQueue"
        ),
        @JMSDestinationDefinition(
            interfaceName = "javax.jms.Topic",
            name = "java:/HELLOWORLDMDBTopic",
            destinationName = "jms.topic.HelloWorldMDBTopic"
        )
    }
)


@WebServlet("/ProducerServlet")
public class ProducerServlet extends HttpServlet {

    private static final long serialVersionUID = -8314035702649252239L;
    private static Logger LOGGER = Logger.getLogger(ProducerServlet.class.toString());

    @Inject
    private JMSContext context;

    @Resource(mappedName = "java:/HELLOWORLDMDBQueue")
    private Queue queue;

    @Resource(mappedName = "java:/HELLOWORLDMDBTopic")
    private Topic topic;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    
        LOGGER.info("In Producer Servlet doGet");

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.write("<h1>POC to test message flow in Queue / Topic</h1>");
        try {
        	
        	String queueMessage = "No Message in Queue";
        	String topicMessage = "No Message in Queue";
        	queueMessage = req.getParameter("queueMessage");
        	topicMessage = req.getParameter("topicMessage");
        	

            out.write("<p>Sending messages to <em> Queue </em></p>");
            out.write("<h2>Message:"+queueMessage+"</h2>");
            sendMessageToQueue(queueMessage) ;
         //   out.write("<p>Sending messages to <em> Topic </em></p>");
            //out.write("<h2>Message:"+topicMessage+"</h2>");
            //sendMessageToTopic(topicMessage);
            
            out.write("<p><i>Go to your Server console or server log to see the result of messages processing.</i></p>");
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    private void sendMessageToQueue(String text) {
    	context.createProducer().send((Destination)queue, text+"");
    }
    
/*    private void sendMessageToTopic(String text) {    
    	context.createProducer().send((Destination)topic, text+"");    	
    }
*/
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}